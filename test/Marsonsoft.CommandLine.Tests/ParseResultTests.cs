﻿using Marsonsoft.CommandLine.Arguments;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Marsonsoft.CommandLine.Tests
{

	[TestClass]
	public class ParseResultTests
	{
		[TestMethod]
		public void ShouldSeparateValuesAndOptions()
		{
			var parser = CreateWindowsParser();

			var result = parser.Parse("/option /optionWithValue:Value value anotherValue thirdValue");

			Assert.AreEqual(5, result.Arguments.Count);

			var options = result.Options();
			Assert.AreEqual(2, options.Count);
			Assert.AreEqual("option", options["option"].Name);
			Assert.AreEqual("Value", ((StringOptionArgument)options["optionWithValue"]).Value);

			var values = result.Values().ToList();
			Assert.AreEqual(3, values.Count);
			Assert.AreEqual("value", values[0]);
			Assert.AreEqual("anotherValue", values[1]);
			Assert.AreEqual("thirdValue", values[2]);
		}

		[TestMethod]
		public void ShouldThrowExceptionOnGetOptionWhenDuplicateOptionName()
		{
			var parser = CreateWindowsParser();

			var result = parser.Parse("/option /option");

			Assert.AreEqual(2, result.Arguments.Count);
			try
			{
				var dictionary = result.Options();
				Assert.Fail("Expected InvalidOperationException");
			}
			catch (InvalidOperationException)
			{
			}
		}

		private CommandLine.Parser CreateWindowsParser()
		{
			var parserSetup = CommandLine.ParserSetup.CreateWindowsStyle();
			return new CommandLine.Parser(parserSetup);
		}
	}
}
