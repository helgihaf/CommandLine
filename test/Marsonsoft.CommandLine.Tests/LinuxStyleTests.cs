﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Marsonsoft.CommandLine.Arguments;

namespace Marsonsoft.CommandLine.Tests
{
	[TestClass]
	public class LinuxStyleTests
	{
		[TestMethod]
		public void ShouldCreateLinuxStyleParser()
		{
			CreateLinuxParser();
		}

		[TestMethod]
		[ExpectedException(typeof(ArgumentNullException))]
		public void ShouldThrowExceptionOnNullParserSetup()
		{
			var parser = new CommandLine.Parser(null);
		}

		[TestMethod]
		public void ShouldParseSingleValue()
		{
			var parser = CreateLinuxParser();
			var result = parser.Parse("hello");
			Assert.AreEqual(1, result.Arguments.Count);
			var valueArg = (ValueArgument)result.Arguments[0];
			Assert.AreEqual("hello", valueArg.Value);
		}

		[TestMethod]
		public void ShouldParseSingleShortOption()
		{
			var parser = CreateLinuxParser();
			var result = parser.Parse("-p");
			Assert.AreEqual(1, result.Arguments.Count);
			var option = (OptionArgument)result.Arguments[0];
			Assert.AreEqual("p", option.Name);
			Assert.AreEqual(typeof(OptionArgument), option.GetType());
		}

		[TestMethod]
		public void ShouldParseSingleLongOption()
		{
			var parser = CreateLinuxParser();
			var result = parser.Parse("--privileged");
			Assert.AreEqual(1, result.Arguments.Count);
			var option = (OptionArgument)result.Arguments[0];
			Assert.AreEqual("privileged", option.Name);
			Assert.AreEqual(typeof(OptionArgument), option.GetType());
		}

		[TestMethod]
		public void ShouldParseSingleShortOptionWithNospaceValue()
		{
			var parser = CreateLinuxParser();
			var result = parser.Parse("-pthevalue");
			Assert.AreEqual(1, result.Arguments.Count);
			var option = (StringOptionArgument)result.Arguments[0];
			Assert.AreEqual("p", option.Name);
			Assert.AreEqual("thevalue", option.Value);
		}

		[TestMethod]
		public void ShouldParseSingleShortOptionWithSpaceValue()
		{
			var parserSetup = CommandLine.ParserSetup.CreateLinuxStyle();
			parserSetup.ShortRequiredValueArguments.Add('p');
			var parser = new CommandLine.Parser(parserSetup);
			var result = parser.Parse("-p thevalue");
			Assert.AreEqual(1, result.Arguments.Count);
			var option = (StringOptionArgument)result.Arguments[0];
			Assert.AreEqual("p", option.Name);
			Assert.AreEqual("thevalue", option.Value);
		}

		[TestMethod]
		public void ShouldParseSingleShortOptionWithNospaceValueFollowedByNormalValue()
		{
			var parserSetup = CommandLine.ParserSetup.CreateLinuxStyle();
			parserSetup.ShortRequiredValueArguments.Add('p');
			var parser = new CommandLine.Parser(parserSetup);
			var result = parser.Parse("-pthevalue normalValue");
			Assert.AreEqual(2, result.Arguments.Count);
			var option = (StringOptionArgument)result.Arguments[0];
			Assert.AreEqual("p", option.Name);
			Assert.AreEqual("thevalue", option.Value);
			var value = (ValueArgument)result.Arguments[1];
			Assert.AreEqual("normalValue", value.Value);
		}

		[TestMethod]
		public void ShouldParseSingleLongOptionWithValue()
		{
			var parser = CreateLinuxParser();
			var result = parser.Parse("--privileged=extremely");
			Assert.AreEqual(1, result.Arguments.Count);
			var option = (StringOptionArgument)result.Arguments[0];
			Assert.AreEqual("privileged", option.Name);
			Assert.AreEqual("extremely", option.Value);
		}

		[TestMethod]
		public void ShouldParseSingleLongOptionWithMultipleValues()
		{
			var parser = CreateLinuxParser();
			var result = parser.Parse("--privileged=extremely,somewhat,rarely");
			Assert.AreEqual(1, result.Arguments.Count);
			var option = (ListOptionArgument)result.Arguments[0];
			Assert.AreEqual("privileged", option.Name);
			Assert.AreEqual(3, option.Values.Count);
			Assert.AreEqual("extremely", option.Values[0]);
			Assert.AreEqual("somewhat", option.Values[1]);
			Assert.AreEqual("rarely", option.Values[2]);
		}

		[TestMethod]
		public void ShouldParseMultipleValues()
		{
			var parser = CreateLinuxParser();
			var result = parser.Parse("hello world");
			Assert.AreEqual(2, result.Arguments.Count);
			Assert.AreEqual("hello", ((ValueArgument)result.Arguments[0]).Value);
			Assert.AreEqual("world", ((ValueArgument)result.Arguments[1]).Value);
		}

		[TestMethod]
		public void ShouldParseMultipleOptions()
		{
			var parser = CreateLinuxParser();
			var result = parser.Parse("-p -h");
			Assert.AreEqual(2, result.Arguments.Count);
			var option = (OptionArgument)result.Arguments[0];
			Assert.AreEqual("p", option.Name);
			Assert.AreEqual(typeof(OptionArgument), option.GetType());
			option = (OptionArgument)result.Arguments[1];
			Assert.AreEqual("h", option.Name);
			Assert.AreEqual(typeof(OptionArgument), option.GetType());
		}

		[TestMethod]
		public void ShouldParseMultipleOptionsWithMultipleValues()
		{
			var parser = CreateLinuxParser();
			var result = parser.Parse("--privileged=extremely,somewhat,rarely --include=foo.h,bar.h");
			Assert.AreEqual(2, result.Arguments.Count);
			var option = (ListOptionArgument)result.Arguments[0];
			Assert.AreEqual("privileged", option.Name);
			Assert.AreEqual(3, option.Values.Count);
			Assert.AreEqual("extremely", option.Values[0]);
			Assert.AreEqual("somewhat", option.Values[1]);
			Assert.AreEqual("rarely", option.Values[2]);
			option = (ListOptionArgument)result.Arguments[1];
			Assert.AreEqual(2, option.Values.Count);
			Assert.AreEqual("foo.h", option.Values[0]);
			Assert.AreEqual("bar.h", option.Values[1]);
		}

		[TestMethod]
		public void ShouldHandleNullStringInput()
		{
			var parser = CreateLinuxParser();
			var result = parser.Parse((string)null);
			Assert.AreEqual(0, result.Arguments.Count);
		}

		[TestMethod]
		public void ShouldHandleNullStringArrayInput()
		{
			var parser = CreateLinuxParser();
			var result = parser.Parse((string[])null);
			Assert.AreEqual(0, result.Arguments.Count);
		}

		[TestMethod]
		public void ShouldHandleEmptyStringArrayInput()
		{
			var parser = CreateLinuxParser();
			var result = parser.Parse(new string[0]);
			Assert.AreEqual(0, result.Arguments.Count);
		}

		[TestMethod]
		public void ShouldHandleEmptyInput()
		{
			var parser = CreateLinuxParser();
			var result = parser.Parse("");
			Assert.AreEqual(0, result.Arguments.Count);
		}

		[TestMethod]
		public void ShouldHandleWhiteSpaceInput()
		{
			var parser = CreateLinuxParser();
			var result = parser.Parse("   \t\t\t \r\n  ");
			Assert.AreEqual(0, result.Arguments.Count);
		}

		[TestMethod]
		public void ShouldParseLongOptionWithEmptyValue()
		{
			var parser = CreateLinuxParser();
			var result = parser.Parse("--include=");
			Assert.AreEqual(1, result.Arguments.Count);
			var option = (StringOptionArgument)result.Arguments[0];
			Assert.AreEqual("include", option.Name);
			Assert.AreEqual("", option.Value);
		}

		[TestMethod]
		public void ShouldSplitCommandLine()
		{
			string commandLine = "ls -a ~";
			string[] args = CommandLine.Splitter.Split(commandLine);
			Assert.AreEqual(3, args.Length);
			int i = 0;
			Assert.AreEqual("ls", args[i++]);
			Assert.AreEqual("-a", args[i++]);
			Assert.AreEqual("~", args[i++]);
		}

		[TestMethod]
		public void ShouldSplitCommandLineWithWhiteSpace()
		{
			string commandLine = "    ls\t\r\n-a\r\n      ~                   \r\n\r\n";
			string[] args = CommandLine.Splitter.Split(commandLine);
			Assert.AreEqual(3, args.Length);
			int i = 0;
			Assert.AreEqual("ls", args[i++]);
			Assert.AreEqual("-a", args[i++]);
			Assert.AreEqual("~", args[i++]);
		}

		[TestMethod]
		public void ShouldSplitCommandLineWithQuotes()
		{
			string commandLine = "ls \"/mnt/c/Program Files\" -a --priveleged=yes";
			string[] args = CommandLine.Splitter.Split(commandLine);
			Assert.AreEqual(4, args.Length);
			int i = 0;
			Assert.AreEqual("ls", args[i++]);
			Assert.AreEqual("/mnt/c/Program Files", args[i++]);
			Assert.AreEqual("-a", args[i++]);
			Assert.AreEqual("--priveleged=yes", args[i++]);
		}

		[TestMethod]
		public void ShouldSplitCommandLineWithAdjacentQuotes()
		{
			string commandLine = "\"/mnt/c/Program Files\" \"/mnt/c/Program Files\" -a --priveleged=yes";
			string[] args = CommandLine.Splitter.Split(commandLine);
			Assert.AreEqual(4, args.Length);
			int i = 0;
			Assert.AreEqual("/mnt/c/Program Files", args[i++]);
			Assert.AreEqual("/mnt/c/Program Files", args[i++]);
			Assert.AreEqual("-a", args[i++]);
			Assert.AreEqual("--priveleged=yes", args[i++]);
		}

		[TestMethod]
		public void ShouldSupportCustomListSeperators()
		{
			var parserSetup = CommandLine.ParserSetup.CreateLinuxStyle();
			parserSetup.ListSeparators.Add("#");

			var parser = new CommandLine.Parser(parserSetup);
			var result = parser.Parse("--privileged=extremely#somewhat#rarely");
			Assert.AreEqual(1, result.Arguments.Count);
			var option = (ListOptionArgument)result.Arguments[0];
			Assert.AreEqual("privileged", option.Name);
			Assert.AreEqual(3, option.Values.Count);
			Assert.AreEqual("extremely", option.Values[0]);
			Assert.AreEqual("somewhat", option.Values[1]);
			Assert.AreEqual("rarely", option.Values[2]);
		}

		[TestMethod]
		public void ShouldSupportMultipleListSeperators()
		{
			var parserSetup = CommandLine.ParserSetup.CreateLinuxStyle();
			parserSetup.ListSeparators.Add(",");
			parserSetup.ListSeparators.Add(";");

			var parser = new CommandLine.Parser(parserSetup);
			var result = parser.Parse("--privileged=extremely,somewhat;rarely");
			Assert.AreEqual(1, result.Arguments.Count);
			var option = (ListOptionArgument)result.Arguments[0];
			Assert.AreEqual("privileged", option.Name);
			Assert.AreEqual(3, option.Values.Count);
			Assert.AreEqual("extremely", option.Values[0]);
			Assert.AreEqual("somewhat", option.Values[1]);
			Assert.AreEqual("rarely", option.Values[2]);
		}

		[TestMethod]
		public void ShouldProcessWindowsStyleAsValues()
		{
			var parser = CreateLinuxParser();
			var result = parser.Parse("/p /privileged:extremely,somewhat,rarely /include:foo.h,bar.h /a:thevalue");
			Assert.AreEqual(4, result.Arguments.Count);
			int index = 0;
			Assert.AreEqual("/p", ((ValueArgument)result.Arguments[index++]).Value);
			Assert.AreEqual("/privileged:extremely,somewhat,rarely", ((ValueArgument)result.Arguments[index++]).Value);
			Assert.AreEqual("/include:foo.h,bar.h", ((ValueArgument)result.Arguments[index++]).Value);
			Assert.AreEqual("/a:thevalue", ((ValueArgument)result.Arguments[index++]).Value);
		}

		[TestMethod]
		public void ShouldBeCaseSensitive()
		{
			var parserSetup = CommandLine.ParserSetup.CreateLinuxStyle();
			parserSetup.ShortRequiredValueArguments.Add('p');
			var parser = new CommandLine.Parser(parserSetup);
			var result = parser.Parse("-P thevalue");
			Assert.AreEqual(2, result.Arguments.Count);
		}

		private CommandLine.Parser CreateLinuxParser()
		{
			var parserSetup = CommandLine.ParserSetup.CreateLinuxStyle();
			return new CommandLine.Parser(parserSetup);
		}
	}
}
