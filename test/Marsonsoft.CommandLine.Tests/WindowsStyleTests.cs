﻿using Marsonsoft.CommandLine.Arguments;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Marsonsoft.CommandLine.Tests
{
	[TestClass]
	public class WindowsStyleTests
	{
		[TestMethod]
		public void ShouldCreateWindowsStyleParser()
		{
			CreateWindowsParser();
		}

		[TestMethod]
		[ExpectedException(typeof(ArgumentNullException))]
		public void ShouldThrowExceptionOnNullParserSetup()
		{
			var parser = new CommandLine.Parser(null);
		}

		[TestMethod]
		public void ShouldParseSingleValue()
		{
			var parser = CreateWindowsParser();
			var result = parser.Parse("hello");
			Assert.AreEqual(1, result.Arguments.Count);
			Assert.AreEqual("hello", ((ValueArgument)result.Arguments[0]).Value);
		}

		[TestMethod]
		public void ShouldParseSingleNoValueOption()
		{
			var parser = CreateWindowsParser();
			var result = parser.Parse("/p");
			Assert.AreEqual(1, result.Arguments.Count);
			var option = (OptionArgument)result.Arguments[0];
			Assert.AreEqual("p", option.Name);
			Assert.AreEqual(typeof(OptionArgument), option.GetType());
		}

		[TestMethod]
		public void ShouldParseSingleLongNameNoValueOption()
		{
			var parser = CreateWindowsParser();
			var result = parser.Parse("/privileged");
			Assert.AreEqual(1, result.Arguments.Count);
			var option = (OptionArgument)result.Arguments[0];
			Assert.AreEqual("privileged", option.Name);
			Assert.AreEqual(typeof(OptionArgument), option.GetType());
		}

		[TestMethod]
		public void ShouldParseSingleOptionWithValue()
		{
			var parser = CreateWindowsParser();
			var result = parser.Parse("/p:thevalue");
			Assert.AreEqual(1, result.Arguments.Count);
			var option = (StringOptionArgument)result.Arguments[0];
			Assert.AreEqual("p", option.Name);
			Assert.AreEqual("thevalue", option.Value);
		}

		[TestMethod]
		public void ShouldParseSingleLongNameOptionWithValue()
		{
			var parser = CreateWindowsParser();
			var result = parser.Parse("/privileged:extremely");
			Assert.AreEqual(1, result.Arguments.Count);
			var option = (StringOptionArgument)result.Arguments[0];
			Assert.AreEqual("privileged", option.Name);
			Assert.AreEqual("extremely", option.Value);
		}

		[TestMethod]
		public void ShouldParseSingleOptionWithMultipleValues()
		{
			var parser = CreateWindowsParser();
			var result = parser.Parse("/privileged:extremely,somewhat,rarely");
			Assert.AreEqual(1, result.Arguments.Count);
			var option = (ListOptionArgument)result.Arguments[0];
			Assert.AreEqual("privileged", option.Name);
			Assert.AreEqual(3, option.Values.Count);
			Assert.AreEqual("extremely", option.Values[0]);
			Assert.AreEqual("somewhat", option.Values[1]);
			Assert.AreEqual("rarely", option.Values[2]);
		}

		[TestMethod]
		public void ShouldParseTurnOptionOn()
		{
			var parser = CreateWindowsParser();
			var result = parser.Parse("/debug+");
			Assert.AreEqual(1, result.Arguments.Count);
			var option = (BoolOptionArgument)result.Arguments[0];
			Assert.AreEqual("debug", option.Name);
			Assert.AreEqual(true, option.Value);
		}

		[TestMethod]
		public void ShouldProcessLinuxStyleAsValues()
		{
			var parser = CreateWindowsParser();
			var result = parser.Parse("-p --privileged=extremely,somewhat,rarely --include=foo.h,bar.h -athevalue");
			int index = 0;
			var values = result.Arguments.Cast<ValueArgument>().Select(a => a.Value).ToArray();
			Assert.AreEqual(4, values.Length);
			Assert.AreEqual("-p", values[index++]);
			Assert.AreEqual("--privileged=extremely,somewhat,rarely", values[index++]);
			Assert.AreEqual("--include=foo.h,bar.h", values[index++]);
			Assert.AreEqual("-athevalue", values[index++]);
		}

		private CommandLine.Parser CreateWindowsParser()
		{
			var parserSetup = CommandLine.ParserSetup.CreateWindowsStyle();
			return new CommandLine.Parser(parserSetup);
		}

	}
}
