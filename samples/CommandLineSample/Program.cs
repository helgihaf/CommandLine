﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Marsonsoft.CommandLine;
using Marsonsoft.CommandLine.Arguments;
using System.IO;

namespace CommandLineSample
{
	class Program
	{
		static void Main(string[] args)
		{
			// Expands file names
			// Usage:
			// CommandLineSample [/u] file1 file2 ...

			var setup = ParserSetup.CreateWindowsStyle();
			var parser = new Parser(setup);
			var result = parser.Parse(args);

			var options = result.Options();

			bool useUrlFormat = options.ContainsKey("u");

			foreach (var fileName in result.Values())
			{
				string filePath = ExpandFileName(fileName);
				if (useUrlFormat)
				{
					filePath = (new Uri(filePath)).AbsoluteUri;
				}
				Console.WriteLine(filePath);
			}
		}

		private static string ExpandFileName(string fileName)
		{
			if (!Path.IsPathRooted(fileName))
			{
				return Path.Combine(Environment.CurrentDirectory, fileName);
			}
			else
			{
				return fileName;
			}
		}
	}
}
