﻿using Marsonsoft.CommandLine.Arguments;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Marsonsoft.CommandLine
{
	/// <summary>
	/// Holds the resule of a Parse operation of the <see cref="Parser"/> class.
	/// </summary>
	public class ParseResult
	{
		/// <summary>
		/// Gets an empty instance of <see cref="ParseResult"/>.
		/// </summary>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2104:DoNotDeclareReadOnlyMutableReferenceTypes")]
		public static readonly ParseResult Empty = new ParseResult(new Argument[0]);

		/// <summary>
		/// Creates an instance of the <see cref="ParseResult"/> using the specified argument list.
		/// </summary>
		/// <param name="arguments">The arguments that make up the parse result.</param>
		public ParseResult(IEnumerable<Argument> arguments)
		{
			if (arguments == null)
			{
				throw new ArgumentNullException(nameof(arguments));
			}
			Arguments = new List<Argument>(arguments);
		}

		/// <summary>
		/// Gets a list of <see cref="Argument"/> objects that were created when parsing.
		/// </summary>
		public IReadOnlyList<Argument> Arguments { get; private set; }

		/// <summary>
		/// Gets the string values created when parsing.
		/// </summary>
		/// <returns>String values.</returns>
		public IEnumerable<string> Values()
		{
			return Arguments.Where(a => a is ValueArgument).Cast<ValueArgument>().Select(v => v.Value);
		}

		/// <summary>
		/// Gets a dictionary of options created when parsing, keyed by the option name.
		/// </summary>
		/// <returns>A dictionary of options.</returns>
		/// <exception cref="InvalidOperationException">Two or more options in the <see cref="Arguments"/> have the same name.</exception>
		public IDictionary<string, OptionArgument> Options()
		{
			try
			{
				return Arguments.Where(a => a is OptionArgument).Cast<OptionArgument>().ToDictionary(opt => opt.Name, opt => opt);
			}
			catch (ArgumentException ex)
			{
				throw new InvalidOperationException("Two or more options have the same name.", ex);
			}
		}
	}
}
