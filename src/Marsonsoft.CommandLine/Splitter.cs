﻿using System.Collections.Generic;

namespace Marsonsoft.CommandLine
{
	/// <summary>
	/// Contains a method to split a command line string into parts.
	/// </summary>
	public static class Splitter
	{
		/// <summary>
		/// Splits a command-line string into parts, supporting quoted strings.
		/// </summary>
		/// <param name="commandLine">The command-line string to split.</param>
		/// <returns>The individual parts of the command-line string.</returns>
		public static string[] Split(string commandLine)
		{
			string[] quoteParts = commandLine.Split('"');
			var list = new List<string>();
			for (int i = 0; i < quoteParts.Length; i++)
			{
				if (i % 2 == 0)
				{
					list.AddRange(SplitUnqoted(quoteParts[i]));
				}
				else
				{
					list.Add(quoteParts[i]);
				}
			}
			return list.ToArray();
		}

		private static IEnumerable<string> SplitUnqoted(string commandLine)
		{
			foreach (var part in commandLine.Split(null))
			{
				var trimmed = part.Trim();
				if (!string.IsNullOrEmpty(trimmed))
				{
					yield return trimmed;
				}
			}
		}
	}
}
