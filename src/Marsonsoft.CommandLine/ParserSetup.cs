﻿using System;
using System.Collections.ObjectModel;

namespace Marsonsoft.CommandLine
{
	/// <summary>
	/// Represents a parser setup used to control some aspects of a <see cref="Parser"/> class.
	/// </summary>
	public class ParserSetup
	{
		/// <summary>
		/// Creates a setup capable of parsing Linux-style command-line arguments.
		/// </summary>
		/// <returns>A Linux-style parser setup.</returns>
		/// <remarks>
		/// A Linux-style parser is case-sensitive and has the following EBNF syntax:
		/// <code>
		/// commandLine:
		/// 	COMMAND { option | value }
		/// 
		/// option:
		/// 		shortOption
		/// 	| 	longOption
		/// 
		/// shortOption:
		/// 	|	"-"LETTER{ }value
		/// 
		/// longOption:
		/// 	"--"WORD["="values]
		/// 
		/// values:
		/// 	value{","value}
		/// </code>
		/// </remarks>
		public static ParserSetup CreateLinuxStyle()
		{
			return new ParserSetup
			{
				ShortOptionMarker = "-",
				LongOptionMarker = "--",
				OptionValueMarker = "=",
				StringComparison = StringComparison.CurrentCulture,
			};
		}

		/// <summary>
		/// Creates a setup capable of parsing Windows-style command-line arguments.
		/// </summary>
		/// <returns>A Windows-style parser setup.</returns>
		/// <remarks>
		/// A Windows-style parser is case-insensitive and has the following EBNF syntax:
		/// <code>
		/// commandLine:
		/// 	COMMAND { option | value }
		/// 
		/// option:
		/// 	"/"WORD[optionArg]
		/// 
		/// optionArg:
		/// 		":" value{","value}
		/// 	|	"+"
		/// 	|	"-"
		/// </code>
		/// </remarks>
		public static ParserSetup CreateWindowsStyle()
		{
			return new ParserSetup
			{
				LongOptionMarker = "/",
				OptionValueMarker = ":",
				OptionOnMarker = "+",
				OptionOffMarker = "-",
				StringComparison = StringComparison.CurrentCultureIgnoreCase,
			};
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="ParserSetup"/> class.
		/// </summary>
		public ParserSetup()
		{
			string currentCultureSeparator = System.Globalization.CultureInfo.CurrentCulture.TextInfo.ListSeparator;
			ListSeparators.Add(",");
			if ("," != currentCultureSeparator)
			{
				ListSeparators.Add(currentCultureSeparator);
			}

			StringComparison = StringComparison.CurrentCulture;
		}

		/// <summary>
		/// Gets or sets the short option marker to use when parsing. Default null. When null, no short options can be parsed.
		/// </summary>
		/// <remark>
		/// A short option marker is used for single-letter options on the command line.
		/// For example, in the following string the "-" string is the short option marker:
		/// "ls -l"
		/// </remark>
		public string ShortOptionMarker { get; set; }

		/// <summary>
		/// Gets or sets the long option marker to use when parsing. Default null. When null, no long options can be parsed.
		/// </summary>
		/// <remarks>
		/// A long option marker is used for word-length options on the command line.
		/// For example, in the following string the "--" string is the long option marker:
		/// "ls --literal"
		/// </remarks>
		public string LongOptionMarker { get; set; }
		
		/// <summary>
		/// Gets or sets the option value marker to use when parsing. Default null. When null no values can be parsed in options.
		/// </summary>
		/// <remarks>
		/// An option value marker is used to separate the option name from the option value.
		/// For exmaple, in the following string the "=" string is the option value marker:
		/// "ls --format=across"
		/// </remarks>
		public string OptionValueMarker { get; set; }
		
		/// <summary>
		/// Gets or sets the option ON marker to use when parsing. Default null. When null, no ON options can be parsed.
		/// </summary>
		/// <remarks>
		/// An option ON marker is used to set a boolean switch value to true. For example, in the following string
		/// the "+" string is the option ON marker:
		/// "csc /debug+ HelloWorld.cs"
		/// </remarks>
		public string OptionOnMarker { get; set; }

		/// <summary>
		/// Gets or sets the option OFF marker to use when parsing. Default null. When null, no OFF options can be parsed.
		/// </summary>
		/// <remarks>
		/// An option OFF marker is used to set a boolean switch value to false. For example, in the following string
		/// the "-" string is the option OFF marker:
		/// "csc /debug- HelloWorld.cs"
		/// </remarks>
		public string OptionOffMarker { get; set; }

		/// <summary>
		/// Gets a collection containing the list of strings that can be used as separators for options that can
		/// accept multiple values. By default, it contains the "," string and the current culture's list separator string (if not ",").
		/// </summary>
		public Collection<string> ListSeparators { get; } = new Collection<string>();

		/// <summary>
		/// Gets a collection containing the characters (letters) that when used as short options will take the next
		/// parsed value as their options value. Default empty.
		/// </summary>
		/// <remarks>
		/// Some single-letter options can have values associated with them. In order for the parser to know that the next
		/// non-option in the command-line is in fact a value of an option instead of a positional value, the parser needs to
		/// know which options to treat this way. The following string shows a command-line using a single-letter option that
		/// requires a value: 
		/// "man -H lynx ls"
		/// </remarks>
		public Collection<char> ShortRequiredValueArguments { get; } = new Collection<char>();

		/// <summary>
		/// Gets or sets the string comparison method used by the parsers. Default <see cref="StringComparison.CurrentCulture"/>.
		/// </summary>
		public StringComparison StringComparison { get; set; }
	}
}