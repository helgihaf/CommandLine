﻿using Marsonsoft.CommandLine.Arguments;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Marsonsoft.CommandLine
{
	/// <summary>
	/// Represents a command-line parser that parses arguments entered on a command-line and returns a 
	/// list of <see cref="Argument"/> objects representing the result.
	/// </summary>
	public class Parser
	{
		private readonly ParserSetup setup;
		private StringComparer stringComparer;

		/// <summary>
		/// Initializes a new instance of the <see cref="Parser"/> class with the specified setup.
		/// </summary>
		/// <param name="setup">A setup object that controls some aspects of the parsing process.</param>
		public Parser(ParserSetup setup)
		{
			if (setup == null)
			{
				throw new ArgumentNullException(nameof(setup));
			}
			this.setup = setup;
		}

		/// <summary>
		/// Parses the specified command-line string.
		/// </summary>
		/// <param name="s">The command-line string to parse.</param>
		/// <returns>The result of parsing <paramref name="s"/></returns>
		/// <remarks>
		/// This method splits the <paramref name="s"/> string into arguments using the <see cref="Splitter"/> class
		/// before commencing with the parsing.
		/// </remarks>
		public ParseResult Parse(string s)
		{
			if (string.IsNullOrWhiteSpace(s))
			{
				return ParseResult.Empty;
			}

			string[] parts = Splitter.Split(s);
			return Parse(parts);
		}

		/// <summary>
		/// Parses the specified arguments.
		/// </summary>
		/// <param name="args">The arguments to parse.</param>
		/// <returns>The result of parsing <paramref name="args"/></returns>
		public ParseResult Parse(string[] args)
		{
			if (args == null)
			{
				return ParseResult.Empty;
			}

			stringComparer = CreateStringComparer(setup.StringComparison);
			var arguments = new List<Argument>();

			foreach (var arg in args)
			{
				string argument = arg.Trim();
				OptionArgument option;

				if (TryParseLongOption(argument, out option))
				{
					arguments.Add(option);
				}
				else if (TryParseShortOption(argument, out option))
				{
					arguments.Add(option);
				}
				else
				{
					if (LastArgumentNeedsValue(arguments))
					{
						int index = arguments.Count - 1;
						var lastArgument = (OptionArgument)arguments[index];
						var newArgument = new StringOptionArgument(lastArgument.Name, argument);
						arguments[index] = newArgument;
					}
					else
					{
						arguments.Add(new ValueArgument(argument));
					}
				}
			}

			return new ParseResult(arguments);
		}

		private static StringComparer CreateStringComparer(StringComparison stringComparison)
		{
			StringComparer result;

			switch (stringComparison)
			{
				case StringComparison.CurrentCultureIgnoreCase:
					result = StringComparer.CurrentCultureIgnoreCase;
					break;
				case StringComparison.InvariantCulture:
					result = StringComparer.InvariantCulture;
					break;
				case StringComparison.InvariantCultureIgnoreCase:
					result = StringComparer.InvariantCultureIgnoreCase;
					break;
				case StringComparison.Ordinal:
					result = StringComparer.Ordinal;
					break;
				case StringComparison.OrdinalIgnoreCase:
					result = StringComparer.OrdinalIgnoreCase;
					break;
				default:
					result = StringComparer.CurrentCulture;
					break;
			}

			return result;
		}

		private bool TryParseLongOption(string argument, out OptionArgument option)
		{
			option = null;

			if (string.IsNullOrEmpty(setup.LongOptionMarker))
			{
				return false;
			}
			int longMarkerLength = setup.LongOptionMarker.Length;

			if (argument.Length <= longMarkerLength || !argument.StartsWith(setup.LongOptionMarker, setup.StringComparison))
			{
				return false;
			}

			string longOption = argument.Substring(longMarkerLength);

			int index = -1;
			if (!string.IsNullOrEmpty(setup.OptionValueMarker))
			{
				index = longOption.IndexOf(setup.OptionValueMarker, setup.StringComparison);
			}

			if (index == -1)
			{
				if (!string.IsNullOrEmpty(setup.OptionOnMarker) && !string.IsNullOrEmpty(setup.OptionOffMarker))
				{
					if (longOption.EndsWith(setup.OptionOnMarker, setup.StringComparison))
					{
						option = new BoolOptionArgument(longOption.Substring(0, longOption.Length - setup.OptionOnMarker.Length), true);
					}
					else if (longOption.EndsWith(setup.OptionOffMarker, setup.StringComparison))
					{
						option = new BoolOptionArgument(longOption.Substring(0, longOption.Length - setup.OptionOffMarker.Length), false);
					}
				}

				if (option == null)
				{
					option = new OptionArgument(longOption);
				}
				return true;
			}

			string name = longOption.Substring(0, index);
			var values = new List<string>();
			if (index + 1 < longOption.Length)
			{
				values.AddRange(ParseValues(longOption.Substring(index + setup.OptionValueMarker.Length)));
			}
			if (values.Count == 0)
			{
				option = new StringOptionArgument(name, "");
			}
			else if (values.Count == 1)
			{
				option = new StringOptionArgument(name, values[0]);
			}
			else
			{
				option = new ListOptionArgument(name, values);
			}
			
			return true;
		}

		private IEnumerable<string> ParseValues(string values)
		{
			return values.Split(setup.ListSeparators.ToArray(), StringSplitOptions.None);

		}

		private bool TryParseShortOption(string argument, out OptionArgument option)
		{
			option = null;

			if (string.IsNullOrEmpty(setup.ShortOptionMarker))
			{
				return false;
			}

			int shortMarkerLength = setup.ShortOptionMarker.Length;

			if (argument.Length <= shortMarkerLength || !argument.StartsWith(setup.ShortOptionMarker, setup.StringComparison))
			{
				return false;
			}

			string name = argument.Substring(shortMarkerLength, 1);
			if (argument.Length >= shortMarkerLength + 2)
			{
				option = new StringOptionArgument(name, argument.Substring(shortMarkerLength + 1));
			}
			else
			{
				option = new OptionArgument(name);
			}
			return true;
		}

		private bool LastArgumentNeedsValue(List<Argument> arguments)
		{
			if (setup.ShortRequiredValueArguments == null || arguments.Count == 0)
			{
				return false;
			}
			var optionArgument = arguments[arguments.Count - 1] as OptionArgument;
			return
				optionArgument != null
				&& optionArgument.GetType() == typeof(OptionArgument)	// thus, has no value yet
				&& optionArgument.Name.Length == 1
				&& setup.ShortRequiredValueArguments.Select(c => c.ToString()).Contains(optionArgument.Name, stringComparer);
		}
	}
}
