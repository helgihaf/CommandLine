﻿using System.Collections.Generic;

namespace Marsonsoft.CommandLine.Arguments
{
	/// <summary>
	/// Represents a named option argument in a command-line that contains multiple string values.
	/// </summary>
	public class ListOptionArgument : OptionArgument
	{
		internal ListOptionArgument(string name, List<string> values)
			: base(name)
		{
			Values = values;
		}

		/// <summary>
		/// Gets a read-only list containing the string values of the option argument.
		/// </summary>
		public IReadOnlyList<string> Values { get; private set; }
	}
}
