﻿namespace Marsonsoft.CommandLine.Arguments
{
	/// <summary>
	/// Represents a named option argument in a command-line.
	/// </summary>
	public class OptionArgument : Argument
	{
		internal OptionArgument(string name)
		{
			Name = name;
		}

		/// <summary>
		/// Gets the name of the option.
		/// </summary>
		public string Name { get; private set; }
	}
}