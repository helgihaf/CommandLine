﻿namespace Marsonsoft.CommandLine.Arguments
{
	/// <summary>
	/// Represents an argument provided in a command-line. This class is abstract.
	/// </summary>
	public abstract class Argument
	{
	}
}
