﻿namespace Marsonsoft.CommandLine.Arguments
{
	/// <summary>
	/// Represents a named option argument in a command-line that has been switched ON or OFF.
	/// </summary>
	public class BoolOptionArgument : OptionArgument
	{
		internal BoolOptionArgument(string name, bool value)
			: base(name)
		{
			Value = value;
		}

		/// <summary>
		/// Gets the ON or OFF value of the option argument.
		/// </summary>
		public bool Value { get; private set; }
	}
}
