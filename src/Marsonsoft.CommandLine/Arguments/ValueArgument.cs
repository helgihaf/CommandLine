﻿namespace Marsonsoft.CommandLine.Arguments
{
	/// <summary>
	/// Represents an un-named argument value (positional value) on a command-line.
	/// </summary>
	public class ValueArgument : Argument
	{
		internal ValueArgument(string value)
		{
			Value = value;
		}

		/// <summary>
		/// Gets the value of the argument.
		/// </summary>
		public string Value { get; private set; }
	}
}
