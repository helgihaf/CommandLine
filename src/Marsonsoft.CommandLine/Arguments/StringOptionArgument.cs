﻿namespace Marsonsoft.CommandLine.Arguments
{
	/// <summary>
	/// Represents a named option argument in a command-line that contains a string value.
	/// </summary>
	public class StringOptionArgument : OptionArgument
	{
		internal StringOptionArgument(string name, string value)
			: base(name)
		{
			Value = value;
		}

		/// <summary>
		/// Gets the string value of the option argument.
		/// </summary>
		public string Value { get; private set; }
	}
}
