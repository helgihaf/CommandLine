## Synopsis

CommandLine is a small library for parsing command-line arguments. It features:
* Linux-style and Windows-style command-line syntax
* Parsing to a list of argument objects representing each item in the command-line
* Support for `--option=value1,value2` multivalue arguments


## Code Example

```C#
static void Main(string[] args)
{
	// Expands file names
	// Usage:
	// CommandLineSample [/u] file1 file2 ...

	var setup = ParserSetup.CreateWindowsStyle();
	var parser = new Parser(setup);
	var result = parser.Parse(args);

	var options = result.Options();

	bool useUrlFormat = options.ContainsKey("u");

	foreach (var fileName in result.Values())
	{
		string filePath = ExpandFileName(fileName);
		if (useUrlFormat)
		{
			filePath = (new Uri(filePath)).AbsoluteUri;
		}
		Console.WriteLine(filePath);
	}
}
```

## License

The project is licensed under the MIT license.